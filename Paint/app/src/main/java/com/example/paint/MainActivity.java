package com.example.paint;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private PaintView paintView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        paintView=findViewById(R.id.paintView);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintView.init(metrics);
        Button btn = findViewById(R.id.button);

        final Spinner spinnerRed = findViewById(R.id.spinnerRed);
        final Spinner spinnerGreen = findViewById(R.id.spinnerGreen);
        final Spinner spinnerBlue = findViewById(R.id.spinnerBlue);

        List<String> red = new ArrayList<>();
        List<String> green = new ArrayList<>();
        List<String> blue = new ArrayList<>();

        for (int i=0;i<256;i++){
            red.add(Integer.toString(i));
            green.add(Integer.toString(i));
            blue.add(Integer.toString(i));
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, red);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRed.setAdapter(dataAdapter);
        spinnerRed.setSelection(100);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, green);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGreen.setAdapter(dataAdapter2);
        spinnerGreen.setSelection(100);

        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, blue);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBlue.setAdapter(dataAdapter3);
        spinnerBlue.setSelection(100);

        spinnerRed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                setColor();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                setColor();
            }

        });

        spinnerGreen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                setColor();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                setColor();
            }

        });

        spinnerBlue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                setColor();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                setColor();
            }

        });

        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String filePath = Environment.getExternalStorageDirectory().toString();
                File file = new File(filePath, "tempFile.png");

                try {
                    OutputStream fOut = new FileOutputStream(file);
                    Bitmap bmp = paintView.getBitmap();
                    bmp.compress(Bitmap.CompressFormat.PNG, 0, fOut);
                    fOut.close();
                    MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filePath + "/tempFile.png"));
                intent.setType("image/png");
                startActivity(intent);

            }});
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            paintView.setmWidth(paintView.getmWidth()-1);
            if (paintView.getmWidth()<1)
                paintView.setmWidth(1);
            Toast.makeText(getApplicationContext(), "Size:" + paintView.getmWidth(), Toast.LENGTH_LONG).show();
        }

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            paintView.setmWidth(paintView.getmWidth()+1);
            Toast.makeText(getApplicationContext(), "Size:" + paintView.getmWidth(), Toast.LENGTH_LONG).show();
        }

        return true;
    }

    public void setColor(){

        Spinner spinnerRed = findViewById(R.id.spinnerRed);
        Spinner spinnerGreen = findViewById(R.id.spinnerGreen);
        Spinner spinnerBlue = findViewById(R.id.spinnerBlue);

        int red = spinnerRed.getSelectedItemPosition();
        String redStr;
        if (red<17)
            redStr = "0" + Integer.toHexString(red);
        else
            redStr = Integer.toHexString(red);

        int green = spinnerGreen.getSelectedItemPosition();
        String greenStr;
        if (green<17)
            greenStr ="0" + Integer.toHexString(green);
        else
            greenStr = Integer.toHexString(green);

        int blue = spinnerBlue.getSelectedItemPosition();
        String blueStr;
        if (blue<17)
            blueStr = "0" + Integer.toHexString(blue);
        else
            blueStr = Integer.toHexString(blue);

        Toast.makeText(getApplicationContext(), "Color in RGB: " + redStr + greenStr + blueStr, Toast.LENGTH_LONG).show();
        paintView.setmColor(Color.parseColor("#" + redStr + greenStr + blueStr));
    }

}
